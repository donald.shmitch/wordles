# Wordles of the world, unite!

This is the source code (Python + YAML) for generating a list of Wordle-like games (https://rwmpelstilzchen.gitlab.io/wordles/).


## How to contribute

[Merge requests](https://gitlab.com/rwmpelstilzchen/wordles/-/merge_requests/) (=*pull request* in GitHub terminology) are the best way to contribute entries. When you make a merge request, please:

* Don’t mix unrelated issues in the same merge request. For example, if you want to fix a typo in one entry and add another entry, make these two merge requests.
* Have a look at the list and the database and make yourself familiar with the basics of how they work and how they are organised.
* Always tick *Allow commits from members who can merge to the target branch*. This way the coordinator can easily commit small changes to make your contribution fit better with the rest of the list.

See the [boost](https://rwmpelstilzchen.gitlab.io/wordles/#boost) section on the project’s page for more information.


## Database structure

The database is made of [YAML](https://en.wikipedia.org/wiki/YAML) files. Each is used by one table on the page, with the exception of `database/languages.yaml`, holds information that is used by several tables.


### `languages.yaml`

- **`iso`** 🔑: Language identification code. If the language has an [ISO 639-3](https://en.wikipedia.org/wiki/ISO_639-3) code, use it. If not, use [Glottolog](https://glottolog.org/)’s codes (with `Glottolog-` prefix). The third best option is [IETF](https://en.wikipedia.org/wiki/IETF_language_tag) language tags (`IETF-` prefix). If none is available, use `nocode-` prefix and make your own identifier. `multilingual` is used in multilingual entries.

- **`native`**: Information about the language in the language itself.
	- **`name`**: The name of the language.
	- **`wiki`** (optional): Wikipedia article.
		- **`title`**: The article’s title.
		- **`lang`**: Language code on Wikipedia (see the URL: `https://XX.wikipedia.org/wiki/ARTICLE_NAME`).

- **`english`**: Information about the language in English.
	- **`name`**: The name of the language.
	- **`wiki`** (optional): Wikipedia article’s title.


#### Order

Entries in this table are sorted by alphabetic order of their `iso` field without prefixes.


### Table files

(this subsubsection is WIP)

- **`name`**: The name of the game.
- **`lang`**: Language of the game. Used in `mlwordles.yaml`, `domain.yaml` and `twist.yaml`
- **`url`**: The URL of the game.
- **`src`** (optional): The URL of the source-code repository.
- **`src-type`** (if there is a `src` field): Type of repository (`github`, `gitlab`, `git` (generic), `glitch`, `pico-8`, or `other`).
- **`announcement`** (optional; currently has no effect on the output): Information regarding the announcement on social media (usually Twitter).
	- **`date`**: In YYYY-MM-DD format.
	- **`url`**.
- **`developer`** (optional; currently has no effect on the output): Information about the developer.
	- **`name`**.
	- **`url`**: Personal website, GitHub page, Twitter account, etc.
- **`reference`** (optional; has no effect on the output): Where did you hear about the game.


#### Order

A hierarchical method is used for sorting entries, as follows:

* **Table**. Each entry appears only once.
* **Language** (where applicable), sorted by the English names. `multilingual` is last.
* **Semantic categorisation** (domain, type of variation, interface/development, object of guessing, etc.; where applicable). These are grouped together; for example, names and codes of organisms are grouped together and mathematical equations are too.
* **Order of insertion**. Within the same category, entries are sorted such that the oldest entries appear first. For many games it takes time to find the exact date they were published, and order of insertion makes a good and fair proxy. `media` about entries (a list of `- url` fields) are sorted in this way as well.

The entries of `lists.yaml`, `media.yaml` and `media-other.yaml` have dates and are sorted by date: `lists.yaml` is oldest first, and `media*.yaml` is newest first.
